jQuery(document).ready(function ($) {
  $('form#login').on('submit', function (e) {
    $('form#login p.status').show().text(ajax_login_object.loadingmessage);

    var data = $(this).serializeArray();
    var dataArray = {
      'action': 'ajaxlogin',
      'username': data[0].value,
      'password': data[1].value,
      'security': data[2].value
    }

    $.post(ajax_login_object.ajaxurl, dataArray,
      function (data) {
        $('#login-alert').removeClass('d-none');
        if (data.loggedin == true) {
          $('#login-alert').addClass('alert-success');
          $('#login-alert').removeClass('alert-danger');

          window.location.href = siteURL + 'ingrese-cv';
        }
        else{
          $('#login-alert').addClass('alert-danger');
          $('#login-alert').removeClass('alert-success');
        }

        $('#login-alert').html(data.message);
      },
      'json'
    );

    e.preventDefault();
  });

  $('form#registerForm').on('submit', function (e) {
    e.preventDefault();

    var data = $(this).serializeArray();
    var dataArray = {
      'action': 'register_user_front_end',
      'new_user_name': data[0].value,
      'new_user_email': data[1].value,
      'new_user_password': data[2].value,
    }

    if(data[2].value != data[3].value) {
      $('#register-alert').removeClass('d-none');
      $('#register-alert').html('Contraseñas no coinciden');

      $('#register-alert').addClass('alert-danger');
      $('#register-alert').removeClass('alert-success');

      return false;
    }

    $.post(registerURL, dataArray)
      .done(function (response) {
        response = $.parseJSON(response);

        $('#register-alert').removeClass('d-none');

        if(response.type == 'error') {
          $('#register-alert').addClass('alert-danger');
          $('#register-alert').removeClass('alert-success');
        }
        else {
          $('#register-alert').addClass('alert-success');
          $('#register-alert').removeClass('alert-danger');

          window.location.href = siteURL + 'ingrese-cv';
        }

        $('#register-alert').html(response.message);
      });
  })
});