<?php
/**
 * Single job listing.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/content-single-job_listing.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     WP Job Manager
 * @category    Template
 * @since       1.0.0
 * @version     1.28.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $post;
?>
<div class="single_job_listing">
	<?php if ( get_option( 'job_manager_hide_expired_content', 1 ) && 'expired' === $post->post_status ) : ?>
		<div class="job-manager-info"><?php _e( 'This listing has expired.', 'wp-job-manager' ); ?></div>
	<?php else : ?>
		<?php
			/**
			 * single_job_listing_start hook
			 *
			 * @hooked job_listing_meta_display - 20
			 * @hooked job_listing_company_display - 30
			 */
			do_action( 'single_job_listing_start' );
		?>

		<div class="job_description">
			<?php wpjm_the_job_description(); ?>
		</div>

		<?php if ( candidates_can_apply() ) : ?>
			<?php get_job_manager_template( 'job-application.php' ); ?>
		<?php endif; ?>

		<?php
			/**
			 * single_job_listing_end hook
			 */
			do_action( 'single_job_listing_end' );
		?>
	<?php endif; ?>

	<?php $tips_array = new WP_Query(array( 'post_type' => 'post', 'posts_per_page' => 5, 'cat' => get_option('tip_category_id') ));
		$tips_array = $tips_array->posts; ?>

	<div class="row footer-job mt-5 py-0">
		<div class="col-4 text-center">
			<img src="<?php echo get_template_directory_uri() ?>/assets/img/man_with_laptop.png" alt="" class="img-fluid">
		</div>
		<div class="col">
			<h3 class="mt-3">Antes de aplicar te recomendamos:</h3>
			<ul class="mt-3">
				<?php foreach($tips_array as $tip): ?>
				<li><a href="<?php echo get_site_url() . '/' . $tip->post_name ?>" class="text-white"><?php echo $tip->post_title ?></a></li>
				<?php endforeach ?>
			</ul>
		</div>
	</div>
	<div class="row share-icons mt-3">
		<div class="col">
			<p>Comparte esta oferta laboral:</p>
			<a href="#" target="_blank"><i class="fab fa-facebook fa-2x"></i></a>
			<a href="#" target="_blank"><i class="fab fa-twitter fa-2x ml-2"></i></a>
			<a href="#" target="_blank"><i class="fab fa-linkedin fa-2x ml-2"></i></a>
			<a href="#" target="_blank"><i class="fab fa-whatsapp fa-2x ml-2"></i></a>
			<a href="#" target="_blank"><i class="fa fa-envelope fa-2x ml-2"></i></a>
		</div>
	</div>
</div>
