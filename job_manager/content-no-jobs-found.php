<?php
/**
 * Notice when no jobs were found in `[jobs]` shortcode.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/content-no-jobs-found.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     WP Job Manager
 * @category    Template
 * @since       1.0.0
 * @version     1.31.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<?php if ( defined( 'DOING_AJAX' ) ) : ?>
	<!-- <li class="no_job_listings_found"><?php esc_html_e( 'There are no listings matching your search.', 'wp-job-manager' ); ?></li> -->
	<div class="row" id="not-found">
		<div class="col-4 offset-1 text-center mt-5">
			<h2>Lo sentimos!</h2>
			<p>En este momento no tenemos la oferta laboral que está buscando<br>Realice una nueva busqueda o <a class="blue" href="#">cargue su hoja de vida</a> para próximos procesos</p>
			<p>También puede registrarse en nuestros boletines semanales.</p>
			<div class="input-group mt-3">
				<input type="text" class="form-control">
				<div class="input-group-append">
					<span class="newsletter input-group-text" id="basic-addon1"><i class="fa fa-arrow-circle-right"></i></span>
				</div>
				<small class="form-text text-muted">Al suscribirse acepta nuestros <a class="blue" href="#">Términos y Condiciones</a></small>
			</div>
		</div>
		<div class="col-6 mt-5">
			<img src="https://via.placeholder.com/600x350" alt="" class="img-fluid">
		</div>
	</div>
<?php else : ?>
	<p class="no_job_listings_found"><?php esc_html_e( 'There are currently no vacancies.', 'wp-job-manager' ); ?></p>
<?php endif; ?>
