<?php get_header(); ?>

  <div class="container">
  <?php $params = array(
    'post_type'=>'post',
    'post_status'=>'publish',
    'posts_per_page'=>-1
  );

  $wpb_all_query = new WP_Query($params);
  if ( $wpb_all_query->have_posts() ) : ?>
  <div class="list-group">
  <?php while ( $wpb_all_query->have_posts() ) :
    $wpb_all_query->the_post();
    $categories = get_the_category(); ?>
    <a class="list-group-item list-group-item-action flex-column align-items-start" href="<?php the_permalink(); ?>">
      <div class="d-flex w-100 justify-content-between">
      <h5 class="mb-1"><span class="badge badge-pill badge-secondary"><?php echo $categories[0]->cat_name ?></span> <?php the_title(); ?></h5>
      <small><?php the_date() ?></small>
      </div>
      <!-- <small>Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</small> -->
    </a>
  <?php endwhile; ?>
  </div>

  <?php wp_reset_postdata(); ?>

  <?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
  <?php endif; ?>
  </div>

<?php get_footer(); ?>