<?php
require_once dirname( __FILE__ ) . '/helpers/TGM-Plugin-Activation-2.6.1/class-tgm-plugin-activation.php';
require_once dirname( __FILE__ ) . '/helpers/class-wp-bootstrap-navwalker.php';

require_once dirname( __FILE__ ) . '/helpers/required-plugins.php';
require_once dirname( __FILE__ ) . '/helpers/theme-settings.php';
require_once dirname( __FILE__ ) . '/helpers/resume-manager-extra.php';

require_once dirname( __FILE__ ) . '/helpers/testimonial-cpt.php';
require_once dirname( __FILE__ ) . '/helpers/testimonial-metabox.php';

define( 'RECAPTCHA_SITE_KEY', '6LcjnpsUAAAAAHG7h7GBHMxNiYlSFMgMRIYM0JnN' );
define( 'RECAPTCHA_SECRET_KEY', '6LcjnpsUAAAAAGX98sYz4Gx-_FtjFvNeKGtYl9Sk' );

add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );
show_admin_bar(false);

register_nav_menus( array(
  'primary' => __( 'Primary Menu', 'emamut' ),
  'footer' => __( 'Footer Menu', 'emamut' ),
) );

function emamut_setup()
{
  load_theme_textdomain( 'emamut' );
}
add_action( 'after_setup_theme', 'emamut_setup' );

function add_theme_scripts()
{
  wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', array(), '1.1', 'all');
  wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', array(), '1.1', 'all');

  wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css');

  wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.3.1.slim.min.js', array (), 1.1, true);
  wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array (), 1.1, true);
  wp_enqueue_script('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array (), 1.1, true);

  wp_enqueue_script('helpers.js', get_template_directory_uri() . '/assets/js/helpers.js', array (), 1.1, true);
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

function ajax_login_init()
{
  wp_register_script('ajax-login-script', get_template_directory_uri() . '/assets/js/ajax-login-script.js', array('jquery') );
  wp_enqueue_script('ajax-login-script');

  wp_localize_script( 'ajax-login-script', 'ajax_login_object', array(
    'ajaxurl' => admin_url( 'admin-ajax.php' ),
    'redirecturl' => home_url(),
    'loadingmessage' => __('Sending user info, please wait...')
  ));

  // Enable the user with no privileges to run ajax_login() in AJAX
  add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
}

if (!is_user_logged_in())
{
  add_action('init', 'ajax_login_init');
}

function ajax_login()
{
  check_ajax_referer( 'ajax-login-nonce', 'security' );

  $info = array();
  $info['user_login'] = $_POST['username'];
  $info['user_password'] = $_POST['password'];
  $info['remember'] = true;

  $user_signon = wp_signon( $info, false );
  if ( is_wp_error($user_signon) )
    echo json_encode(array('loggedin'=>false, 'message'=>__('Nombre de usuario o contraseña incorrecta!')));
  else
    echo json_encode(array('loggedin'=>true, 'message'=>__('Acceso correcto, redireccionando...')));

  die();
}

add_action('wp_ajax_register_user_front_end', 'register_user_front_end', 0);
add_action('wp_ajax_nopriv_register_user_front_end', 'register_user_front_end');

function register_user_front_end()
{
  $new_user_name = stripcslashes($_POST['new_user_name']);
  $new_user_email = stripcslashes($_POST['new_user_email']);
  $new_user_password = $_POST['new_user_password'];
  $user_nice_name = strtolower($_POST['new_user_email']);
  $user_data = array(
    'user_login' => $new_user_name,
    'user_email' => $new_user_email,
    'user_pass' => $new_user_password,
    'user_nicename' => $user_nice_name,
    'display_name' => $new_user_first_name,
    'role' => 'subscriber'
  );

  $user_id = wp_insert_user($user_data);
  $response = array('type' => 'success', 'message' => '');

  if (!is_wp_error($user_id))
  {
    $response['message'] = 'Cuenta creada exitosamente.';
    wp_set_current_user($user_id);
    wp_set_auth_cookie($user_id);
  }
  else
  {
    $response['type'] = 'error';

    if (isset($user_id->errors['empty_user_login']))
      $response['message'] = 'Usuario y correo son obligatorios';
    elseif (isset($user_id->errors['existing_user_login']))
      $response['message'] = 'Usuario ya existe.';
    else
      $response['message'] = 'Por favor llene el formulario cuidadosamente.';
  }

  echo json_encode($response);
  die;
}

if ( function_exists ('register_sidebar')) {
  register_sidebar( array(
    'name'          => __('Lateral'),
    'id'            => 'primary',
    'description'   => 'Sidebar for showing ad and section list on the template single-job.php',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>' )
  );
}

add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
  wp_redirect( home_url() );
  exit();
}

// Enqueue Google reCAPTCHA scripts
add_action( 'wp_enqueue_scripts', 'recaptcha_scripts' );
function recaptcha_scripts() {
	wp_enqueue_script( 'recaptcha', 'https://www.google.com/recaptcha/api.js' );
}

// Add reCAPTCHA to the job submission form
// If you disabled company fields, the submit_job_form_end hook can be used instead from version 1.24.1 onwards
add_action( 'submit_job_form_company_fields_end', 'recaptcha_field' );
function recaptcha_field() {
	?>
	<fieldset>
		<label>Are you human?</label>
		<div class="field">
			<div class="g-recaptcha" data-sitekey="<?php echo RECAPTCHA_SITE_KEY; ?>"></div>
		</div>
	</fieldset>
	<?php
}

// Validate
add_filter( 'submit_job_form_validate_fields', 'validate_recaptcha_field' );
function validate_recaptcha_field( $success ) {
	$response = wp_remote_get( add_query_arg( array(
		'secret'   => RECAPTCHA_SECRET_KEY,
		'response' => isset( $_POST['g-recaptcha-response'] ) ? $_POST['g-recaptcha-response'] : '',
		'remoteip' => isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']
	), 'https://www.google.com/recaptcha/api/siteverify' ) );
	if ( is_wp_error( $response ) || empty( $response['body'] ) || ! ( $json = json_decode( $response['body'] ) ) || ! $json->success ) {
		return new WP_Error( 'validation-error', '"Are you human" check failed. Please try again.' );
	}
	return $success;
}

add_action( 'submit_resume_form_resume_fields_end', 'recaptcha_field' );
add_filter( 'submit_resume_form_validate_fields', 'validate_recaptcha_field' );
add_action( 'job_application_form_fields_end', 'recaptcha_field' );
add_filter( 'application_form_validate_fields', 'validate_recaptcha_field' );