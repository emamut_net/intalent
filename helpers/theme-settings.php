<?php
function theme_option_page()
{ ?>
  <div class="wrap">
    <h1>Theme Customization</h1>
    <form method="post" action="options.php">
      <?php settings_fields("theme-options-grp");
      do_settings_sections("theme-options");
      submit_button(); ?>
    </form>
  </div>
<?php }

function add_theme_menu_item()
{
  add_theme_page("Theme Customization", "Theme Customization", "manage_options", "theme-options", "theme_option_page", null, 99);
}
add_action("admin_menu", "add_theme_menu_item");

function main_picture_callback()
{ ?>
  <label for="upload_image">
    <input id="upload_image" type="text" size="36" name="main_picture" value="<?php echo get_option('main_picture'); ?>" />
    <input id="upload_image_button" class="button" type="button" value="Upload Image" />
    <br />Enter a URL or upload an image
  </label>
<?php }

function display_small_text()
{ ?>
  <textarea name="small_text" id="small_text" cols="50" rows="3"><?php echo get_option('small_text'); ?></textarea>
<?php }

function display_yt_id()
{ ?>
  <input name="yt_id" id="yt_id" type="text" size="50" value="<?php echo get_option('yt_id'); ?>">
<?php }

function display_tip_category_id()
{
  $categories = get_categories( array(
    'orderby' => 'name',
    'order'   => 'ASC'
  ) ); ?>
  <select name="tip_category_id" id="tip_category_id">
    <option value="">-- Elija una categoría --</option>
    <?php foreach ($categories as $category): ?>
      <option value="<?php echo $category->term_id ?>" <?php if (get_option('tip_category_id') == $category->term_id) echo 'selected'; ?>><?php echo $category->name ?></option>
    <?php endforeach ?>
  </select>
<?php }

function display_big_text()
{ ?>
  <textarea name="big_text" id="big_text" cols="50" rows="3"><?php echo get_option('big_text'); ?></textarea>
<?php }

function theme_settings()
{
  add_settings_section( 'first_section', 'Main Image', '', 'theme-options');

  add_settings_field('main_picture', 'Image', 'main_picture_callback', 'theme-options', 'first_section');
  register_setting( 'theme-options-grp', 'main_picture');

  add_settings_field('small_text', 'Small Text', 'display_small_text', 'theme-options', 'first_section');
  register_setting( 'theme-options-grp', 'small_text');

  add_settings_field('big_text', 'Big Text', 'display_big_text', 'theme-options', 'first_section');
  register_setting( 'theme-options-grp', 'big_text');

  add_settings_section( 'second_section', 'Youtube Video', '', 'theme-options');

  add_settings_field('yt_id', 'Youtube ID', 'display_yt_id', 'theme-options', 'second_section');
  register_setting( 'theme-options-grp', 'yt_id');

  add_settings_section( 'third_section', 'Tips Category', '', 'theme-options');

  add_settings_field('tip_category_id', 'Category', 'display_tip_category_id', 'theme-options', 'third_section');
  register_setting( 'theme-options-grp', 'tip_category_id');
}
add_action('admin_init', 'theme_settings');

add_action('admin_enqueue_scripts', 'my_admin_scripts');

function my_admin_scripts()
{
  wp_enqueue_media();
  wp_register_script('my-admin-js', get_template_directory_uri() . '/helpers/my-admin.js', array('jquery'));
  wp_enqueue_script('my-admin-js');
}