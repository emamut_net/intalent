<?php get_header(); ?>

  <div class="container">
    <div class="row">
      <div class="col">
      <?php while ( have_posts() ) : the_post();?>
        <div class="page-header">
          <small>Publicado: <?php the_time('F jS, Y') ?></small>
        </div>

        <?php the_post_thumbnail( 'full', ['class' => 'img-fluid'] ) ?>

        <div class="entry">
          <?php the_content('Read the rest of this entry &raquo;'); ?>
        </div>
      <?php endwhile; ?>
      </div>
      <?php if ( is_active_sidebar( 'primary' ) AND is_singular('job_listing' ) ): ?>
        <?php get_sidebar( 'primary' ); ?>
      <?php endif ?>
    </div>
  </div>

<?php get_footer(); ?>
