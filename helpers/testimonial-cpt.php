<?php
// Register Custom Post Type
function testimonial()
{
  $labels = array(
    'name'                => _x( 'Testimonios', 'Post Type General Name', 'testimonial' ),
    'singular_name'       => _x( 'Testimonio', 'Post Type Singular Name', 'testimonial' ),
    'menu_name'           => __( 'Testimonios', 'testimonial' ),
    'name_admin_bar'      => __( 'Testimonios', 'testimonial' ),
    'parent_item_colon'   => __( 'Parent Item:', 'testimonial' ),
    'all_items'           => __( 'Todos Los Testimonios', 'testimonial' ),
    'add_new_item'        => __( 'Añadir Nuevo Testimonio', 'testimonial' ),
    'add_new'             => __( 'Añadir Nuevo', 'testimonial' ),
    'new_item'            => __( 'Nuevo Testimonio', 'testimonial' ),
    'edit_item'           => __( 'Editar Testimonio', 'testimonial' ),
    'update_item'         => __( 'Actualizar Testimonio', 'testimonial' ),
    'view_item'           => __( 'Ver Testimonio', 'testimonial' ),
    'search_items'        => __( 'Buscar Testimonio', 'testimonial' ),
    'not_found'           => __( 'No encontrado', 'testimonial' ),
    'not_found_in_trash'  => __( 'No encontrado en papelera', 'testimonial' ),
  );

  $args = array(
    'label'                 => __( 'testimonial', 'testimonial' ),
    'description'           => __( 'Slider Custom Post Type', 'testimonial' ),
    'labels'                => $labels,
    'supports'              => array( 'author', 'revisions', 'thumbnail', 'editor', 'title' ),
    'hierarchical'          => false,
    'public'                => false,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-megaphone',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'               => false,
    'capability_type'       => 'post',
    'show_in_rest'          => true,
    'rest_base'             => 'slider-api',
    'rest_controller_class' => 'WP_REST_Posts_Controller'
  );
  register_post_type( 'testimonial', $args );
}
add_action( 'init', 'testimonial', 0 );