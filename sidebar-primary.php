<div id="primary" class="col-12 col-md-4 sidebar">
    <?php do_action( 'before_sidebar' ); ?>
    <div class="card border-light col special mb-3">
      <div class="card-body text-white">
        <h6 class="pt-5">Estamos para ayudarte</h6>
        <h2 class="card-title text-white"><strong>Fundación InTalent</strong></h2>
        <p class="card-text">It may comfort you to know that Fry's death took only fifteen seconds, yet the pain was so intense, that it felt to him like fifteen years.</p>
      </div>
      <div class="card-footer text-center">
        <button class="btn btn-default btn-lg shadow-sm">CONOCER MÁS</button>
      </div>
    </div>
    <?php if ( ! dynamic_sidebar( 'primary' ) ) : ?>
      <?php echo get_sidebar('primary');
    endif; ?>
</div>