<?php
function testimonial_register_meta_boxes($meta_boxes)
{
  $prefix = 'testimonial_';

  $meta_boxes[] = array(
    'id'         => 'testimonial',
    'title'      => 'Testimonios',
    'post_types' => 'testimonial',
    'context'    => 'normal',
    'priority'   => 'high',
    'fields'     => array(
      array(
        'name'              => 'Cargo',
        'id'                => $prefix . 'position',
        'type'              => 'text'
      )
    )
  );

  return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'testimonial_register_meta_boxes' );