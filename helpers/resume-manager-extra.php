<?php
add_filter( 'submit_resume_form_fields', 'custom_submit_resume_form_fields' );
function custom_submit_resume_form_fields( $fields )
{
  $fields['resume_fields']['candidate_education']['fields']['location']['label'] = 'Institución';
  $fields['resume_fields']['candidate_education']['fields']['date']['label'] = 'Fecha de inicio';
  $fields['resume_fields']['candidate_education']['fields']['qualification']['label'] = 'Título de la carrera';

  $fields['resume_fields']['candidate_experience']['fields']['employer']['label'] = 'Empresa';
  $fields['resume_fields']['candidate_experience']['fields']['job_title']['label'] = 'Cargo o puesto';
  $fields['resume_fields']['candidate_experience']['fields']['date']['label'] = 'Fecha de inicio';

  return $fields;
}

// add_filter( 'resume_manager_resume_fields', 'custom_resume_manager_resume_fields' );
// function custom_resume_manager_resume_fields( $fields ) {
//   //  $fields['_candidate_location']['placeholder'] = "Custom placeholder";
//   $fields['resume_education_location']['label'] = 'Institución';

//   return $fields;
// }

add_filter( 'resume_manager_resume_education_fields', 'wpjms_admin_resume_form_fields' );
function wpjms_admin_resume_form_fields( $fields ) {
  $fields['education_end_date'] = array(
    'label' => __( 'Fecha de finalización', 'job_manager' ),
    'name' => 'resume_education_end_date[]',
    'type' => 'text',
    'placeholder' => '',
    'description'	=> '',
    'priority' => 1
  );

  return $fields;
}

add_filter( 'resume_manager_resume_experience_fields', 'wpjms_admin_resume_form_fields' );
function wpjms_admin_resume_form_fields2( $fields ) {
  $fields['experience_end_date'] = array(
    'label' => __( 'Fecha de finalización', 'job_manager' ),
    'name' => 'resume_experience_end_date[]',
    'type' => 'text',
    'placeholder' => '',
    'description'	=> '',
    'priority' => 1
  );

  return $fields;
}

add_filter( 'submit_resume_form_fields', 'wpjms_frontend_resume_form_fields' );
function wpjms_frontend_resume_form_fields( $fields ) {
  $fields['resume_fields']['candidate_education']['fields']['end_date'] = array(
      'label' => __( 'Fecha de finalización', 'job_manager' ),
      'type' => 'text',
      'required' => true,
      'placeholder' => '',
      'priority' => 1
  );

  $fields['resume_fields']['candidate_experience']['fields']['end_date'] = array(
      'label' => __( 'Fecha de finalización', 'job_manager' ),
      'type' => 'text',
      'required' => true,
      'placeholder' => '',
      'priority' => 1
  );

  return $fields;
}