<?php
$post_array = new WP_Query(array(
  'post_type' => 'post',
  'posts_per_page' => 3
));
$post_array = $post_array->posts;

foreach ($post_array as $post)
{
  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );
  $post->featured_image = $image[0];

  $post->categories = get_the_category( $post->ID );
  $post->special_date = date('d, F Y', strtotime($post->post_date));
} ?>

  <section class="mt-5" id="news">
    <div class="container text-white">
      <div class="row">
        <div class="col-12 col-md-3 px-5 my-md-3 py-3">
          <span>Intalent News</span>
          <h2 class="mt-3">Editorial, noticias y eventos</h2>
        </div>
        <?php foreach($post_array as $post): ?>
        <div class="col-12 col-md-3 px-5 my-md-3 py-3">
          <span><?php echo $post->categories[0]->name ?></span>
          <h4 class="mt-3"><a href="<?php echo get_site_url() . '/' . $post->post_name ?>"><?php echo $post->post_title ?></a></h4>
          <p class="date mt-5 text-uppercase">
            <i class="fa fa-calendar-alt"></i> <?php echo $post->special_date ?>
          </p>
        </div>
        <?php endforeach ?>
      </div>
    </div>
  </section>

  <?php $logos = scandir(dirname(__FILE__) . '/assets/img/clients'); unset($logos[0], $logos[1]); ?>
  <section id="clients" class="container my-5">
    <div class="row justify-content-center align-items-center">
      <?php foreach ($logos as $logo): ?>
      <div class="col col-md-2">
        <img src="<?php echo get_template_directory_uri() ?>/assets/img/clients/<?php echo $logo ?>" alt="" class="img-fluid">
      </div>
      <?php endforeach ?>
    </div>
  </section>

  <footer class="mt-3 py-5 text-white">
    <div class="container">
      <div class="row border-bottom border-light">
        <div class="col-12 col-md-4">
          <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-intalent-white.png" alt="" class="img-fluid mb-3">
          <h5 class="mt-3">Dirección</h5>
          <address>
            Parque Empresarial Colón<br>
            Edificio Pacific Plaza - Oficina 110<br>
            Guayaquil, Ecuador
          </address>

          <h5 class="mt-3">¿Conversamos?</h5>
          <p class="phone-number"><a href="https://wa.me/593959246377" target="_blank" style="color:#ffffff;">+5939-59246377</a></p>
        </div>
        <div class="col-12 col-md-4">
          <h5>Acerca de InTalent</h5>
          <?php
            wp_nav_menu( array(
              'theme_location'    => 'footer',
              'depth'             => 1,
              'container'         => 'ul',
              'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
              'walker'            => new WP_Bootstrap_Navwalker(),
            ) ); ?>
        </div>
        <div class="col-12 col-md-4">
          <h5>Suscríbase a nuestro boletín</h5>
          Déjenos su correo electrónico para estar informado de nuestras novedades, procesos de selección y más noticias del area de Recursos Humanos.
          <div class="input-group mt-3">
            <input type="text" class="form-control">
            <div class="input-group-append">
              <span class="newsletter input-group-text" id="basic-addon1"><i class="fa fa-arrow-circle-right"></i></span>
            </div>
            <small class="form-text text-muted">Al suscribirse acepta nuestros <a class="blue" href="#">Términos y Condiciones</a></small>
          </div>

          <h5 class="mt-4">Visite nuestra redes sociales</h5>
          <a href="https://www.facebook.com/Intalentec-1908422366127163/?ref=br_rs" target="_blank" class="white"><i class="fab fa-facebook fa-2x pr-2"></i></a>
          <a href="#" target="_blank" class="white"><i class="fab fa-twitter fa-2x pr-2"></i></a>
          <a href="https://www.instagram.com/intalent.ec/" target="_blank" class="white"><i class="fab fa-instagram fa-2x pr-2"></i></a>
          <a href="#" target="_blank" class="white"><i class="fab fa-linkedin fa-2x pr-2"></i></a>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col">
          Derechos reservados <?php echo date("Y") ?> &copy; Human Resources Asesoría Davitalent S.A
        </div>
        <div class="col-4 align-self-end">
          UX Design <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-holos-white.png" alt="" class="img-fluid">
        </div>
      </div>
    </div>
  </footer>

  <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="container">
            <div class="row my-3">
              <div class="col-6 d-none d-md-block">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/bg-modal.png" alt="" class="img-fluid">
              </div>
              <div class="col">
                <ul class="nav nav-pills flex-column flex-sm-row" id="loginTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true">INICIAR SESIÓN</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="register-tab" data-toggle="tab" href="#register" role="tab" aria-controls="register" aria-selected="false">REGISTRARSE</a>
                  </li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="login" role="tabpanel" aria-labelledby="login-tab">
                    <form id="login">
                      <div class="form-group mt-3">
                        <input type="text" name="username" class="form-control" placeholder="Ingrese su usuario ó correo electrónico" autocomplete="off" required>
                      </div>

                      <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Ingrese su contraseña" required>
                      </div>

                      <button type="submit" class="btn btn-primary btn-block">INGRESAR Y APLICAR</button>
                        <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
                    </form>
                    <div class="alert d-none mt-3" id="login-alert"></div>
                  </div>
                  <div class="tab-pane" id="register" role="tabpanel" aria-labelledby="register-tab">
                    <form id="registerForm">
                      <div class="form-group mt-3">
                        <input type="text" name="new_user_name" class="form-control" placeholder="Ingrese su usuario" autocomplete="off" required>
                      </div>

                      <div class="form-group">
                        <input type="text" name="new_user_email" class="form-control" placeholder="Ingrese su correo electrónico" autocomplete="off" required>
                      </div>

                      <div class="row">
                        <div class="form-group col-6">
                          <input type="password" name="new_user_password" class="form-control" placeholder="Ingrese su contraseña" required>
                        </div>

                        <div class="form-group col-6">
                          <input type="password" name="re_pwd" class="form-control" placeholder="Repetir su contraseña" required>
                        </div>
                      </div>

                      <button type="submit" class="btn btn-primary btn-block">REGISTRAR Y CARGAR HOJA DE VIDA</button>
                    </form>

                    <div class="alert d-none mt-3" id="register-alert"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php wp_footer(); ?>
  </body>
</html>