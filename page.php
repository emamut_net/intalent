<?php get_header(); ?>

  <div class="container">
    <?php
      while ( have_posts() ) : the_post(); ?>
        <div class="entry-content-page">
          <?php the_content(); ?>
        </div>
      <?php endwhile;
      wp_reset_query(); ?>
  </div>

<?php get_footer() ?>