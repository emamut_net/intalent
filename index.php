<?php get_header();
  $main_picture = get_option('main_picture');
  if(!$main_picture)
    $main_picture = get_template_directory_uri() . '/assets/img/intalent-header.jpg';

  $testimonials_array = new WP_Query(array( 'post_type' => 'testimonial' ));
  $testimonials_array = $testimonials_array->posts;

  foreach ($testimonials_array as $post)
  {
    $custom_fields = get_post_custom($post->ID);

    $post->position = $custom_fields['testimonial_position'][0];
    $post->image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
  } ?>

  <section id="main-image" class="container-fluid">
    <div class="row">
      <img src="<?php echo $main_picture ?>" alt="" class="img-fluid">
      <div class="container">
        <div class="text col-md-6 col-xs-12">
          <p><?php echo get_option('small_text') ?></p>
          <h4><?php echo get_option('big_text') ?></h4>
          <a href="<?php echo get_site_url() ?>/reclutamiento-y-seleccion-de-personal" class="btn btn-light btn-lg shadow-sm mt-md-5 mr-3">EMPRESAS</a>
          <a href="<?php echo get_site_url() ?>/bolsa-de-trabajo" class="btn btn-primary btn-lg shadow-sm mt-3 mt-md-5">CANDIDATOS</a>
        </div>
      </div>
    </div>
  </section>

  <section id="services" class="container mt-5">
    <div class="row justify-content-center">
      <div class="card border-light col-md-4 col-xs-12 regular">
        <div class="image-filter">
          <img src="<?php echo get_template_directory_uri() ?>/assets/img/intalent-seleccion-block.png" class="card-img">
        </div>
        <div class="card-img-overlay">
          <h4 class="card-header text-white"><strong>Reclutamiento y Selección</strong></h5>
        </div>
        <div class="card-body">
          <p class="card-text">Utilizamos metodologías innovadoras para el reclutamiento de talentos y la evaluación de competencias.</p>
        </div>
        <div class="card-footer row">
          <div class="col">
            <h5><a href="<?php echo get_site_url() ?>/reclutamiento-y-seleccion-de-personal">VER MÁS</a></h5>
          </div>
          <div class="col text-right">
            <i class="fa fa-arrow-right"></i>
          </div>
        </div>
      </div>

      <div class="card border-light col-md-4 col-xs-12 regular">
        <div class="image-filter">
          <img src="<?php echo get_template_directory_uri() ?>/assets/img/intalent-capacitacion-block.png" class="card-img">
        </div>
        <div class="card-img-overlay">
          <h4 class="card-header text-white"><strong>Capacitación y Formación</strong></h4>
        </div>
        <div class="card-body">
          <p class="card-text">Programas avalados para el desarrollo profesional de competencias humanas y comerciales.</p>
        </div>
        <div class="card-footer row">
          <div class="col">
            <h5><a href="<?php echo get_site_url() ?>/capacitaciones">VER MÁS</a></h5>
          </div>
          <div class="col text-right">
            <i class="fa fa-arrow-right"></i>
          </div>
        </div>
      </div>

      <div class="card border-light col-md-4 col-xs-12 special">
        <div class="card-body text-white">
          <h6 class="pt-5">Estamos para ayudarte</h6>
          <h2 class="card-title text-white"><strong>Fundación Intalent</strong></h2>
          <p class="card-text">Contamos con diversos proyectos sociales para ayudar en la inserción laboral de grupos vulnerables.</p>
        </div>
        <div class="card-footer text-center">
          <a href="<?php echo get_site_url() ?>/fundacion-intalent" class="btn btn-default btn-lg shadow-sm">CONOCER MÁS</a>
        </div>
      </div>
    </div>
  </section>

  <section id="video">
    <div class="container mt-5">
      <div class="row justify-content-center py-5">
        <div class="col-md-8 col-xs-12">
          <h2 class="text-center">Somos un equipo profesional con más de <strong>10 años de experiencia,</strong> apasionados por nuestro trabajo</h2>
        </div>
        <div class="w-100"></div>
        <div class="col-md-2 col-xs-4 text-center mt-5 video-player">
          <img src="<?php echo get_template_directory_uri() ?>/assets/img/ic-playbutton.png" alt="" class="img-fluid" data-toggle="modal" data-target="#videoModal">
          <p>Vea nuestro<br> video</p>
        </div>
      </div>
    </div>
  </section>

  <section id="metodologia" class="container mt-5">
    <div class="row">
      <div class="col-md-7">
        <h2><br>Metodología Intalent</h2>
        <p>Hemos creado una metodología que piensa en el colaborador seleccionado y en sus oportunidades de formación.</p>
        <ul class="mt-4 list-unstyled">
          <li class="media mb-5">
            <img class="mr-3" src="<?php echo get_template_directory_uri() ?>/assets/img/ic-intalentcheck.png" alt="">
            <div class="media-body">
              <h3>Buenos resultados</h3>
            </div>
          </li>
          <li class="media mb-5">
            <img class="mr-3" src="<?php echo get_template_directory_uri() ?>/assets/img/ic-intalentcheck.png" alt="">
            <div class="media-body">
              <h3>Compromiso hacia la empresa</h3>
            </div>
          </li>
          <li class="media mb-5">
            <img class="mr-3" src="<?php echo get_template_directory_uri() ?>/assets/img/ic-intalentcheck.png" alt="">
            <div class="media-body">
              <h3>Actitudes positivas</h3>
            </div>
          </li>
          <li class="media mb-5">
            <img class="mr-3" src="<?php echo get_template_directory_uri() ?>/assets/img/ic-intalentcheck.png" alt="">
            <div class="media-body">
              <h3>Bajos niveles de rotación</h3>
            </div>
          </li>
        </ul>
        <div class="row justify-content-center">
          <a href="<?php echo get_site_url() ?>/reclutamiento-y-seleccion-de-personal" class="btn btn-primary btn-lg shadow-sm">Me Interesa</a>
        </div>
      </div>
      <div class="col-md-5">
        <div id="metodologiaCarousel" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <?php foreach($testimonials_array as $key => $testimonial): ?>
            <li data-target="#metodologiaCarousel" data-slide-to="<?php echo $key ?>" <?php if($key == 0) echo 'class="active"' ?>></li>
            <?php endforeach ?>
          </ol>
          <div class="carousel-inner">
            <?php foreach($testimonials_array as $key => $testimonial): ?>
            <div class="carousel-item <?php if($key == 0) echo 'active' ?>">
              <img src="<?php echo $testimonial->image[0] ?>" alt="" class="w-100">
              <div class="carousel-caption d-none d-md-block">
                <div class="testimonial"><?php echo $testimonial->post_content ?></div>
                <div class="name">
                  <h5><?php echo $testimonial->post_title ?></h5>
                  <p><?php echo $testimonial->position ?></p>
                </div>
              </div>
            </div>
            <?php endforeach ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="embed-responsive embed-responsive-4by3">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo get_option('yt_id') ?>?rel=0" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>

<?php get_footer() ?>