<?php require_once 'helpers/bootstrap-breadcrumb.php' ?>

<!DOCTYPE html>
  <html lang="<?php language_attributes(); ?>" class="no-js">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?php wp_head(); ?>
  </head>
  <body>
    <script>
      var siteURL = '<?php echo get_site_url() ?>/';
      var registerURL = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>

    <a id="return-to-top"><i class="fa fa-chevron-up"></i></a>

    <nav id="primary" class="navbar navbar-expand-md navbar-light fixed-top bg-light">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="<?php echo get_site_url() ?>">
        <img id="logo" src="<?php echo get_template_directory_uri() ?>/assets/img/logo-intalent.svg" alt="">
      </a>
      <?php
        wp_nav_menu( array(
          'theme_location'    => 'primary',
          'depth'             => 2,
          'container'         => 'div',
          'container_class'   => 'collapse navbar-collapse justify-content-end',
          'container_id'      => 'navbarCollapse',
          'menu_class'        => 'nav navbar-nav',
          'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
          'walker'            => new WP_Bootstrap_Navwalker(),
        ) );

        if ( is_user_logged_in() )
          $current_user = wp_get_current_user(); ?>
        <ul class="nav navbar-nav">
          <?php if ( (isset($current_user) AND $current_user instanceof WP_User) ): ?>
          <li class="nav-item dropdown user-dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php echo get_avatar( $current_user->user_email, 32 ) . '<span class="ml-2">Hola, ' . $current_user->display_name . '</span>' ?>
            </a>
            <div class="dropdown-menu" aria-labelledby="userDropdown">
              <a href="<?php echo get_site_url(); ?>/ingrese-cv" class="dropdown-item">Editar mi hoja de vida</a>
              <a href="<?php echo get_site_url(); ?>/dashboard-candidato" class="dropdown-item">Mis postulaciones</a>
              <a href="<?php echo wp_logout_url(); ?>" class="dropdown-item">Cerrar sesión</a>
            </div>
          </li>
          <?php else: ?>
          <li class="nav-item without-border">
            <a id="triggerLoginModal" href="#" class="nav-link btn btn-outline-primary">Registrar mi hoja de vida</a>
          </li>
          <?php endif ?>
        </ul>
    </nav>

    <?php if(!is_home()): ?>
      <div class="container">
        <div class="position-relative">
          <?php bootstrap_breadcrumb() ?>
          <h2 id="page-header"><?php echo get_the_title() ?></h2>
        </div>
      </div>
    <?php endif ?>