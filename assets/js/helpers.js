jQuery(document).ready(function ($) {
  $(window).scroll(function () {
    if ($(this).scrollTop() >= 50) {
      $('#logo').width('50%');
      $('#primary .nav').css('margin-top', '0');
      $('#return-to-top').fadeIn(200);
    }
    else {
      $('#logo').width('100%');
      $('#primary .nav').css('margin-top', '3rem');
      $('#return-to-top').fadeOut(200);
    }
  });

  $('#return-to-top').click(function () {
    $('body,html').animate({
      scrollTop: 0
    }, 1000);
  });

  $('#triggerLoginModal')
    .on('click', function (e) {
      e.preventDefault();
      $('#loginModal').modal('show')
    });
});